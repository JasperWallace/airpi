import output
import datetime
import graphiteclient

class Print(output.Output):
	requiredData = []
	optionalData = ["server", "port"]

	def __init__(self, data):
		print(data)
		server = "graphite.lan.pointless.net"
		port = 2003
		if "server" in data:
			server = data["server"]

		if "port" in data:
			port = int(data["port"])
			
		self.client = graphiteclient.GraphiteClient(server=server, port=port)
		
	def outputData(self,dataPoints):
		# key, value
		out = {}
		should_see = {"ABM_713_RC.Volume":1, "DHT22.Relative_Humidity":1, "DHT22.Temperature":1}
#		print ""
#		print "Time: " + str(datetime.datetime.now())
		for i in dataPoints:#
			# symbol
			# sensor
			# unit
			# value
			# name
			key = i["sensor"] + "." + i["name"]
			out[key] = i["value"]
#			print  + " = " +  + " " + i["symbol"]
#			print(str(i))
#			s = m["sensor"] + "_" + m["name"]
		sks = should_see.keys()
		for k in sks:
			if k not in out:
				out[k] = 0
		
		ks = out.keys()
		base = "env.desk.airpi"
		for k in ks:
			metric = "%s.%s" % (base, k)
#			print "%s %s" % (metric, str(out[k]))
			self.client.poke(metric, out[k])

		print "count: %d" % (len(ks))
		print ""
		return True
