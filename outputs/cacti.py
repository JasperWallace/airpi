import output
import datetime
import threading, Queue, time
import BaseHTTPServer, urlparse, urllib

class Cacti(output.Output):
	requiredData = []
	optionalData = []
	def __init__(self,data):
		self.queue = Queue.Queue(42)
		self.http = threading.Thread(name="webserver", target=self.run_server)
		self.http.setDaemon(True)
		self.http.start()
		
	def run_server(self):
		httpd = BaseHTTPServer.HTTPServer(("", 12345), Handler)
		httpd.queue = self.queue
		httpd.serve_forever()

	def outputData(self,dataPoints):
		for i in dataPoints:
#			print i["name"] + ": " + str(i["value"]) + " " + i["symbol"]
#			print i["sensor"] + "_" + i["name"] + ": " + str(i["value"])
			self.queue.put(dataPoints)
		print
		return True

class Handler(BaseHTTPServer.BaseHTTPRequestHandler):
  def address_string(self):
    return str(self.client_address[0])

  def log_message(self, format, *args):
  	print self.address_string() + " " + format % args
#    logger.info(self.address_string() + " " + format % args)

  def do_GET(self):
    url = urlparse.urlparse(self.path)
    params = urlparse.parse_qs(url.query)
    path = url.path
    print "HTTP: " + self.path
    if path == "/favicon.ico":
      return

    if path != "/":
      self.send_response(404)

      self.send_header('Content-type', 'text/plain')
      self.end_headers()
      self.wfile.write(path + " not found.\n")
      return

    self.send_response(200)
    self.send_header('Content-type', 'text/plain')
    self.end_headers()

    data = []

    try:
      while True:
      	# each entry is an array of hashes
        data.append(self.server.queue.get(block=False))
    except Queue.Empty:
    	pass

	# sensor : array
	measurements = {}

	for d in data:
		for m in d:
			s = m["sensor"] + "_" + m["name"]
			if s not in measurements:
				measurements[s] = [m["value"],]
			else:
				measurements[s].append(m["value"])
	
	ks = measurements.keys()
	# this sensor dosn't measure any voltage if it's quiet.
	# change resistor values? play with op-amp?
	if "ABM_713_RC_Volume" not in ks:
		ks.append("ABM_713_RC_Volume")
		measurements["ABM_713_RC_Volume"] = [0,]
        if "DHT22_Relative_Humi" not in ks:
          ks.append("DHT22_Relative_Humi")
          measurements["DHT22_Relative_Humi"] = [0,]
          
        if "DHT22_Temperature" not in ks:
          ks.append("DHT22_Temperature")
          measurements["DHT22_Temperature"] = [0,]

	ks.sort()
#	self.wfile.write(":".join(ks) + "\n")
	out = []
	for i, k in enumerate(ks):
		out.append(k + ":" + str(sum(measurements[k]) / len(measurements[k])))
#		out.append(sum(measurements[k]) / len(measurements[k]))
	self.wfile.write(" ".join(out) + "\n")
